; docformat = 'rst'
;
;+
;
; :Purpose:
;   Convert a time string into a Julian date
;
; :Inputs:
;   time_in: (input, string, required) Time of some format to be specified
;   format: (input, string, required) Format describing time string.
;     See case statement to see available options
;
; :Keywords:
;   hour: (input, optional, integer) Hours to be added to the date
;   minute: (input, optional, integer) Minutes to be added to the date
;   start_time: (input, optional, integer) For the format string MONTHS SINCE,
;     a Julian date is required, specifying the 'since' date.
;
; :Outputs:
;   A Julian date
;
; :Example::
;   IDL> time=mr_time_convert('1/1/2000', 'M/D/Y')
;   IDL> caldat, time, month, day, year
;   IDL> print, month, day, year
;     1
;     1
;     2000
;   
; :History:
; 	Written by: Matt Rigby, University of Bristol, Jul 16, 2013
;
;-
Function mr_time_convert, time_in, format, hour=hour, minute=minute, start_time=start_time

  on_error, 2
  compile_opt idl2, hidden

  TimeSize=n_elements(time_in)
  if keyword_set(hour) eq 0 then hour=replicate(0L, TimeSize)
  if keyword_set(minute) eq 0 then minute=replicate(0L, TimeSize)

  month3=['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']

  Case strupcase(format) of
    'YYYYMMDD': begin
      time_in=strtrim(string(reform(time_in)), 2)
      year=fix(strmid(time_in, 0, 4))
      month=fix(strmid(time_in, 4, 2))
      day=fix(strmid(time_in, 6, 2))
      time_out=julday(month, day, year, fix(hour), fix(minute), 0)
    end
    'YYYY-MM-DD': begin
      time_in=strtrim(string(reform(time_in)), 2)
      year=fix(strmid(time_in, 0, 4))
      month=fix(strmid(time_in, 5, 2))
      day=fix(strmid(time_in, 8, 2))
      time_out=julday(month, day, year, fix(hour), fix(minute), 0)
    end
    'YYYY-MM-DD HH:MM:SS': begin
      time_in=strtrim(string(reform(time_in)), 2)
      year=fix(strmid(time_in, 0, 4))
      month=fix(strmid(time_in, 5, 2))
      day=fix(strmid(time_in, 8, 2))
      hour=fix(strmid(time_in, 11, 2))
      minute=fix(strmid(time_in, 14, 2))
      seconds=fix(strmid(time_in, 17, 2))      
      time_out=julday(month, day, year, hour, minute, seconds)
    end
    'YYMMDD': begin
      time_in=strtrim(string(reform(time_in)), 2)
      year=fix(strmid(time_in, 0, 2))
      wh=where(year gt 30)
      if wh[0] ne -1 then year[wh]=1900+year[wh]
      wh=where(year le 30)
      if wh[0] ne -1 then year[wh]=2000+year[wh]
      
      month=fix(strmid(time_in, 2, 2))
      day=fix(strmid(time_in, 4, 2))
      time_out=julday(month, day, year, fix(hour), fix(minute), 0)
    end
    'YYYYMM': begin
      time_in=strtrim(string(reform(long(time_in))), 2)
      year=fix(strmid(time_in, 0, 4))
      month=fix(strmid(time_in, 4, 2))
      time_out=julday(month, 15, year, fix(hour), fix(minute), 0)
    end
    'DD.MM.YYYY': begin
      time_in=strtrim(string(reform(time_in)), 2)
      year=fix(strmid(time_in, 6, 4))
      month=fix(strmid(time_in, 3, 2))
      day=fix(strmid(time_in, 0, 2))
      time_out=julday(month, day, year, fix(hour), fix(minute), 0)
    end
    'M/D/Y': begin
      time_out=dblarr(TimeSize)
      time_in=strtrim(string(reform(time_in)), 2)
      for ti=0, TimeSize-1 do begin
        month=fix(strmid(time_in[ti], 0, strpos(time_in[ti], '/')))
        time_in_temp=strmid(time_in[ti], strpos(time_in[ti], '/')+1, strlen(time_in[ti]))
        day=fix(strmid(time_in_temp, 0, strpos(time_in_temp, '/')))
        time_in_temp=strmid(time_in_temp, strpos(time_in_temp, '/')+1, strlen(time_in_temp))
        if strlen(time_in_temp) eq 2 then begin
          if fix(time_in_temp) lt 30 then year=2000 + fix(time_in_temp) else year=1900 + fix(time_in_temp)
        endif else begin
          year=fix(time_in_temp)
        endelse
        time_out[ti]=julday(month, day, year, fix(hour[ti]), fix(minute[ti]), 0)        
      endfor
    end
    'D/M/Y': begin
      time_in=strtrim(string(reform(time_in)), 2)
      day=fix(strmid(time_in, 0, strpos(time_in, '/')))
      time_in=strmid(time_in, strpos(time_in, '/')+1, strlen(time_in))
      month=fix(strmid(time_in, 0, strpos(time_in, '/')))
      time_in=strmid(time_in, strpos(time_in, '/')+1, strlen(time_in))
      if strlen(time_in) eq 2 then begin
        if fix(time_in) lt 30 then year=2000 + fix(time_in) else year=1900 + fix(time_in)
      endif else begin
        year=fix(time_in)
      endelse
      time_out=julday(month, day, year, fix(hour), fix(minute), 0)
    end
    'MM/DD/YYYY': begin
      time_in=strtrim(string(reform(time_in)), 2)
      year=fix(strmid(time_in, 6, 4))
      month=fix(strmid(time_in, 0, 2))
      day=fix(strmid(time_in, 3, 2))
      time_out=julday(month, day, year, fix(hour), fix(minute), 0)
    end
    'YYMMM': begin
      time_in=strtrim(string(reform(time_in)), 2)
      year=fix(strmid(time_in, 0, 2))
      wh=where(year gt 50, count)
      if count gt 0 then year[wh]+=1900
      wh=where(year lt 50, count)
      if count gt 0 then year[wh]+=2000
      month_temp=strmid(time_in, 2, 3)
      month=intarr(n_elements(month_temp))
      for mi=0, n_elements(month)-1 do begin
        month[mi]=where(month3 eq month_temp[mi])+1
      endfor
      time_out=julday(month, 15, year, 0, 0, 0)
    end    
    'MONTHS SINCE': begin
      if n_elements(start_time) eq 0 then begin
        message, 'NEED TO DEFINE START TIME'
      endif else begin
        caldat, start_time, Start_month, Start_day, Start_year
        year_add=fix(round(time_in)/12L)
        month=round(time_in) + Start_month - 12*year_add
        time_out=julday(month, Start_day, Start_year + year_add, 0, 0)
      endelse
    end
  EndCase

  return, time_out
  
end
