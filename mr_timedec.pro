; docformat = 'rst'
;
;+
;
; :Purpose:
;   Convert Julian date to decimal date, or visa-versa
;   Currently hourly resolution only
;
; :Inputs:
;   Time: (float, double or long) either a Julian date to convert to a decimal date, 
;     in which case a number above 100000 is expected, or a decimal date to be converted
;     to a Julian date, in which ase a number below 100000 is expected.
;
; :Example::
; 
;   Convert Julian date to decimal date
;   IDL> print, mr_timedec(julday(1, 1, 2000, 0))
;   IDL> 2000.0000
;
;   Convert decimal date to Julian date
;   IDL> print, mr_timedec(2000.)
;   IDL> 2451544.5
;
;
; :History:
; 	Written by: Matt Rigby, University of Bristol, Jul 6, 2013
;
;-
function mr_timedec, time

  compile_opt idl2, hidden

  if double(time[0]) gt 100000.d then begin
    caldat, time, month, day, year
    timedec=dblarr(n_elements(time))
    daysinyear=double(365L+((year mod 4) eq 0))
    jan1=julday(1, 1, year, 0, 0)
    timedec=double(year) + (time - jan1)/daysinyear
  endif else begin
    daysinyear=double(365L+((fix(time) mod 4) eq 0))
    hoursinyear=double(365L+((fix(time) mod 4) eq 0))*24.d
    days = fix( ( time - float(fix(time)) )*daysinyear )
    hours = round( ( time - float(fix(time)) - double(days)/daysinyear ) * hoursinyear )
    timedec=julday(1, 1+days, fix(time), hours, 0)
  endelse

  return, timedec

End