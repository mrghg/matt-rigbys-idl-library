; docformat = 'rst'
;
; NAME:
;   FileStr
;
; PURPOSE:
;   Append user-specific working directory to front of file string.
;   Correct slash/backslash for unix/windows systems.
;   Remove any spaces in string.
;
;+
; :Returns:
;   File string formatted for operating system, with working directory prefixed
;
; :Requires:
;   !mr_working_directory system variable. 
;   E.g. add this to your startup script:
;     defsysv, '!MR_WORKING_DIRECTORY', "/my/working/directory"
;
; :Params:
;   Ext : string variable containing file/directory to which directory prefix should be prefixed
;
; :Examples:
;    IDL> full_path=FILESTR('/dir/filename')
;    IDL> print, full_path
;    /Users/matt/Work/dir/filename
;
; :History:
;   Written by: Matt Rigby, MIT, May 31st 2011
;
;-
function mr_filestr, ext

  compile_opt idl2, hidden
  on_error, 2
  
  if n_elements(!mr_working_directory) eq 0 then begin
    message, "Set !mr_working_directory in your startup script. " + $
      'E.g. defsysv, "!MR_WORKING_DIRECTORY", "/my/working/directory"'
  endif

  if !version.os_family eq 'Windows' then begin
    pos=strpos(ext, '/')
    If pos ne -1 then begin
      while pos ne -1 do begin
        strput, ext, '\', pos
        pos=strpos(ext, '/')
      Endwhile
    Endif
    filestr=Strcompress(!mr_working_directory + ext, /remove_all)
  endif else begin
    pos=strpos(ext, '\')
    If pos ne -1 then begin
      while pos ne -1 do begin
        strput, ext, '/', pos
        pos=strpos(ext, '\')
      Endwhile
    Endif
    filestr=strcompress(!mr_working_directory + ext, /remove_all)
  Endelse

  return, filestr

End