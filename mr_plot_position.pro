; docformat = 'rst'
;
;+
;
; :Purpose:
;   Generate an array that can be input to the plot position keyword when multiple plots are required. 
;   The output can be input to the plot procedure to produce equally-size, equally-spaced plots.
;   
; :Inputs:
;   x_plot: the number of plots in the x direction
;   y_plot: the number of plots in the y direction
;
; :Keywords:
;   Gap: (input, default=0.05) the gap between plots in both directions
;   x_Gap: (input, default=0.05) the gap between plots in x direction, cannot be used with GAP keyword
;   y_Gap: (input, default=0.05) the gap between plots in y direction, cannot be used with GAP keyword
;   
; :Outputs:
;   An N x 4 array of positions, where N is the total number of plots.  The first dimension of the array is
;   the plot number going top-left to top-right and then down.  The second dimension are the four positions 
;   that the plot routine requires in the POSITION keyword.
;   
; :Example::
;   To create a 3x2 array of plots:
;   
;     IDL> position=plot_position(3, 2, gap=0.1)::
;     IDL> print, position::
;       0.0500000     0.383333     0.716667    0.0500000     0.383333     0.716667
;       0.550000     0.550000     0.550000    0.0500000    0.0500000    0.0500000
;       0.283333     0.616667     0.950000     0.283333     0.616667     0.950000
;       0.950000     0.950000     0.950000     0.450000     0.450000     0.450000
;     IDL> for pi=0, 5 do plot, x, y, position=position[pi, *]
;     
; :History:
; 	Written by: Matt Rigby, MIT, Aug 19, 2011
;
;-
Function mr_plot_position, x_plots, y_plots, gap=gap, right=right, left=left, top=top, bottom=bottom, x_gap=x_gap, y_gap=y_gap

  if keyword_Set(bottom) eq 0 then bottom=0.05
  if keyword_Set(top) eq 0 then top=0.05
  if keyword_Set(left) eq 0 then left=0.05
  if keyword_Set(right) eq 0 then right=0.05
  if keyword_set(gap) then begin
    y_Gap=gap
    x_gap=gap  
  endif else begin
    if keyword_set(x_gap) eq 0 then x_Gap=0.05
    if keyword_set(y_gap) eq 0 then y_Gap=0.05
  endelse
  
  width=(1. - left - right - float(x_plots - 1)*x_gap)/float(x_plots)
  height=(1. - top - bottom - float(y_plots - 1)*y_gap)/float(y_plots)

  position=fltarr(x_plots*y_plots, 4)
  
  ;position from top left, top right, down
  For yi=0, y_plots-1 do begin
    for xi=0, x_plots-1 do begin
      position[xi + x_plots*yi, 0]=left + float(xi)*(width + x_gap)
      position[xi + x_plots*yi, 1]=bottom + float(y_plots-yi-1)*(height + y_gap)
      position[xi + x_plots*yi, 2]=left + float(xi)*(width + x_gap) + width
      position[xi + x_plots*yi, 3]=bottom + float(y_plots-yi-1)*(height + y_gap) + height
    endfor  
  endfor

  return, position

End



