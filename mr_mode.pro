; docformat = 'rst'
;
; NAME:
;   MODE
;
;+
; :Purpose:
;   Calculate modal average
;
; :Inputs:
;   Array : Array of values to average
;      
; :Examples:
;   IDL> a=[1., 2., 2., 2., 5.]
;   IDL> b=mr_mode(a)
;   IDL> print, b
;     2.00000
;
; :History:
;   Written by: Matt Rigby, MIT, May 31st 2011
;
;-
function mr_mode, array

  ;Calculate unique values
  arrayU=array[uniq(array, sort(array))]
  
  ;Number of elements that match unique values
  count=lonarr(n_elements(arrayU))
  for Ui=0, n_elements(arrayU)-1 do count[ui]=long(total(array eq arrayU[ui]))
  
  dummy=max(count, maxI)
  mode=arrayU[MaxI]
  
  return, mode

end