; docformat = 'rst'
;
;+
;
; :Purpose:
;   Creates input to polygon procedure or function, removing non-finte elements
; 
; :Inputs:
;   X: x values
;   Y_low: low limit of polygon
;   Y_high: high limit of polygon
;   
; :Outputs:
;
; :Example::
;   poly=mr_polygon(x, y_low, y_high)
;   pl=polygon(poly.x, poly.y)
;
; :History:
; 	Written by: Matt Rigby, University of Bristol, Apr 15, 2013
;
;-
function mr_polygon, X, Y_low, Y_high

  polyY=[reform(Y_low), reverse(reform(Y_high))]
  wh=where(finite(polyY), count)
  if count gt 0 then begin
    polyY=polyY[wh]
  endif else begin
    message, 'No finite elements'
  endelse

  polyX=[reform(X), reverse(reform(X))]
  polyX=polyX[wh]
  
  return, {X: polyX, Y: polyY}

end


