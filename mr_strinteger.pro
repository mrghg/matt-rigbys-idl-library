; docformat = 'rst'
;
;+
;
; :Purpose:
;   Formats numbers into string integers with rounding
;
; :Inputs:
;   Any number
;
; :Outputs:
;
; :Example::
;   print, mr_strInteger(1.5)
;   
; :History:
; 	Written by: Matt Rigby, University of Bristol, Jan 28, 2013
;
;-

function mr_strinteger, input

  return, string(round(input), format='(I)')

end