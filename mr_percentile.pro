; docformat = 'rst'
;
;+
;
; :Purpose:
;   Calculate a particular percentile of input array
;
; :Inputs:
;   array_in: data array
;   percentile: percentile to calculate
;
; :Example::
; 
;   IDL> x=findgen(100)
;   IDL> print, percentile(x, 40)
;     40.00000
;
; :History:
; 	Written by: Matt Rigby, University of Bristol, Jul 16, 2013
;
;-
function mr_percentile, array_in, percentile

  compile_opt idl2, hidden

  array=array_in[sort(array_in)]
  output=array[float(percentile)/100.*n_elements(array)]

  return, output

end