; docformat = 'rst'
;
;+
;
; :Purpose:
;   Either change a MOZART-format date ('YYYYMMDD') to Julian date, or visa versa
;
; :Inputs:
;   Either a MOZART date string, or a Julian date
; 
; :Outputs:
;   date='20001020'
;
; :Example::
;   time=mr_mozdate(date)
;
; :History:
; 	Written by: Matt Rigby, University of Bristol, Nov 25, 2013
;
; :Modification:
;   Edited by AS to cope with date strings that are only 6 characters long. Defaults to the 15th of the month
;-
function mr_mozdate, date
 
  if size(date, /type) eq 4 or size(date, /type) eq 5 then begin
    caldat, date, m1, d1, y1
    
    mozdate=long(y1)*10000L + long(m1)*100L + long(d1)

  endif else begin
    
    date=strtrim(string(date), 2)
    
    year=long(strmid(date, 0, 4))
    month=long(strmid(date, 4, 2))
    if mean(strlen(date)) ne 8 then day=replicate(15l, n_elements(date)) else day =long(strmid(date, 6, 2))
    
    mozdate=dblarr(n_elements(date))
    
    For n=0, n_elements(date)-1 do begin
      mozdate[n]=julday(month[n], day[n], year[n], 0, 0, 0)
    endfor

  endelse
  
  return, mozdate

End