; docformat = 'rst'
;
;+
;
; :Purpose:
;   Find index of closet grid cell
;
; :Inputs:
;   Vector: grid point locations
;   location: point of interest
;
; :Outputs:
;   array index of closet grid cell
;   
; :Example::
;   index=mr_nearest(vector, 50.)
;   
; :History:
; 	Written by: Matt Rigby, University of Bristol, Apr 2, 2014
;
;-
function mr_nearest, vector, location

  distance=abs(vector - location)
  dummy=min(distance, wh)
  return, wh

end