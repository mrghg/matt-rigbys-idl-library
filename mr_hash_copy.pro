; docformat = 'rst'
;
;+
;
; :Purpose:
;   Copy a hash table to a new variable, without a connection between the two hashes.
;   For example, if:
;     a=hash()
;     b=a
;   Any changes made to b will also be reflected in a!
;   Whereas if you do:
;     a=hash()
;     b=mr_hash_copy(a)
;   you can now change b, without the corresponding elements of a changing
;
;   WARNING! will only descend two levels!
;
; :Inputs:
;   Hash table
;
; :Outputs:
;   Copied hash
;   
; :Example::
;   a=hash()
;   b=mr_hash_copy(a)
;
; :History:
; 	Written by: Matt Rigby, University of Bristol, Jul 16, 2013
;
;-
function mr_hash_copy, original

  compile_opt idl2, hidden

  copy=hash()
  foreach variable, original, key do begin
    if typeName(original[key]) eq 'HASH' then begin
      copy[key]=original[key, *]
    endif else begin
      copy[key]=original[key]
    endelse
  endforeach

  return, copy
  
end