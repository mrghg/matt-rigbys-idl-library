; docformat = 'rst'
;
;+
;
; :Purpose:
;   Passes a hash table to a IDL_IDLbridge object.
;   Currently descends two hash levels (if more levels are required, will need re-writing)
;
; :Inputs:
;   var: Hash variable to pass
;   obj: IDL_IDLbridge object
;
; :Example::
; 
;
; :History:
; 	Written by Matt Rigby, University of Bristol, August 2012,
;     based on struct_pass.pro by J. Arnold & R. da Silva, UCSC
;     http://slugidl.pbworks.com/w/page/28915050/Slug%20IDL%20Code
;
;-
;
pro mr_hash_pass, var, obj

  compile_opt idl2, hidden

  varname=scope_varname(var, level=-1)
  varKeys=var.keys()

  fluff='fluff'

  obj->execute, varname + '=hash()'

  for j=0,n_elements(varKeys)-1 do begin
    
    Case typename(var[varkeys[j]]) of
      'HASH': begin
        if typename(varKeys[j]) eq 'STRING' then begin
          firstLevStr="['" + string(varKeys[j]) + "'"
        endif else begin
          firstLevStr="[" + string(varKeys[j]) + ""
        endelse
        obj->execute, varname + firstLevStr + "]=hash()"
        varSub=var[varkeys[j]]
        varSubKeys=varSub.keys()
        for k=0, n_elements(varSubKeys)-1 do begin
          obj->setvar, fluff, varSub[varSubKeys[k]]
          if typename(varSubKeys[k]) eq 'STRING' then begin
            obj->execute, varname + firstLevStr + ", '" + string(varSubKeys[k]) + "']=" + fluff
          endif else begin
            obj->execute, varname + firstLevStr + ", " + string(varSubKeys[k]) + "]=" + fluff
          endelse
        endfor
      end

      else: begin
        obj->setvar, fluff, var[varKeys[j]]
        if typename(varKeys[j]) eq 'STRING' then begin
          obj->execute, varname + "['" + string(varKeys[j]) + "']=" + fluff
        endif else begin
          obj->execute, varname + "[" + string(varKeys[j]) + "]=" + fluff
        endelse
      end
    endcase
  endfor
  
  obj->execute, fluff + '=!null'

end
