; docformat = 'rst'
;
;+
;
; :Purpose:
;   Calculate ODP tonnes from metric tonnes, or visa-versa
;
; :Inputs:
;   input: Emissions in Gg (default), or emissions in ODP tonnes
;
; :Requires:
;   mr_ODP.csv file ozone depletion potentials
;
; :Keywords:
;   MP: Use Montreal Protocol-adopted values, instead of Daniel et al., 2011
;   Reverse: Calculate metric tonnes from ODP tonnes
; 
; :Outputs:
;   ODP-weighted emissions
;
; :Example::
;   emissions_ODP=mr_ODP(emissions, 'CFC-12')
;
; :History:
;   Written by: Matt Rigby, University of Bristol, Aug 27, 2013
;
;-
function mr_odp, input, pollutant, MP=MP, reverse=reverse
  

  ;Get ozone depletion potentials
  data=read_csv(file_which('mr_ODP.csv'), record_start=2)
  pollutantRef=reform(data.(0))
  ODP_WMO=float(reform(data.(1)))
  ODP_MP=float(reform(data.(2)))

  if keyword_set(MP) then begin
    ODP=ODP_MP
  endif else begin
    ODP=ODP_WMO
  endelse

  wh=where(pollutantRef eq pollutant, count)

  if count gt 0 then begin
    if keyword_set(reverse) then begin
      output=input/ODP[wh[0]]
    endif else begin
      output=input*ODP[wh[0]]
    endelse
  endif else begin
    output=0.*input
  endelse

  return, output

end