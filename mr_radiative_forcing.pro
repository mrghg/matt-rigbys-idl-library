function mr_radiative_forcing_f, m, n
  f=0.47*alog(1.+2.01E-5*(M*N)^(0.75)+5.31E-15*M*(M*N)^(1.52))
  return, f
end
; docformat = 'rst'
;
;+
;
; :Purpose:
;   Calculate radiative forcing using IPCC (2001) formulea
;   CO20, CH40 and N2O0 from http://www.esrl.noaa.gov/gmd/aggi/
;   
; :Inputs:
;
; :Requires:
;
; :Outputs:
;   Radiative forcing in mW/m2
;   
; :Example::
;   rf=mr_radiative_forcing(c, 'CH4')
;
; :History:
; 	Written by: Matt Rigby, University of Bristol, Apr 24, 2013
;
;-
function mr_radiative_forcing, mole_fraction, pollutant

  c=mole_fraction

  data=read_csv(file_which('mr_radiative_efficiency.csv'), record_start=2)
  pollutantRef=reform(data.(0))
  alphaRef=float(reform(data.(1)))
  C0Ref=float(reform(data.(2)))
  
  wh=where(pollutantRef eq pollutant, count)
  if count gt 0 then begin
    alpha=alphaRef[wh]
    C0=C0Ref[wh]
  endif else begin
    message, "Can't find " + pollutant + " in mr_radiative_efficiency.csv"
  endelse

  if pollutant eq 'CH4' then begin
    CH40=C0
    wh=where(pollutantRef eq 'N2O', count)
    if count gt 0 then begin
      N2O0=C0Ref[wh]
    endif else begin
      message, "Can't find N2O in mr_radiative_efficiency.csv. Required for CH4 RF."
    endelse
  endif

  if pollutant eq 'N2O' then begin
    N2O0=C0
    wh=where(pollutantRef eq 'CH4', count)
    if count gt 0 then begin
      CH40=C0Ref[wh]
    endif else begin
      message, "Can't find CH4 in mr_radiative_efficiency.csv. Required for N2O RF."
    endelse
  endif

  case pollutant of
    'CO2': begin
      dF=alpha[0]*alog(C/C0[0])*1000.
    end
    'CH4': begin
      dF=(alpha[0]*(sqrt(C) - sqrt(CH40[0])) - $
        ( mr_radiative_forcing_f(C, N2O0[0])- mr_radiative_forcing_f(CH40[0], N2O0[0])))*1000.
    end
    'N2O': begin
      dF=(alpha[0]*(sqrt(C) - sqrt(N2O0[0])) - $
        ( mr_radiative_forcing_f(CH40[0], C) - mr_radiative_forcing_f(CH40[0], N2O0[0])))*1000.
    end
    else: begin
      dF=alpha[0]*(C - C0[0])
    end
    endcase

    return, dF

end