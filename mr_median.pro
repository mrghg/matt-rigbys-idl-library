; docformat = 'rst'
;
;+
;
; :Purpose:
;   Calculate median, with the ability to remove NaNs
;
; :Inputs:
;   Array
;
; :Keywords:
;   NaN: set to true to ignore NaNs
;
; :Outputs:
;   Median of input array
;
; :Example::
;
; :History:
; 	Written by: Matt Rigby, University of Bristol, Jul 16, 2013
;
;-
function mr_median, input, nan=nan

  compile_opt idl2, hidden

  output=input[sort(input)]
  
  if keyword_set(nan) then begin
    output=output[where(finite(output))]
  endif

  output=output[0.5*n_elements(output)]

  return, output

end